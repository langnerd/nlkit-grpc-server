from core.async_data_pipeline import AsyncDataPipeline
from proto.nlp_pb2 import *
from typing import List
import spacy

step = AsyncDataPipeline.step


class AsyncSpacyService:
    def __init__(self):
        self.pipeline = AsyncDataPipeline(sinks=[
            step(self.__get_entities)
        ])

    # def GetEntities(self, request, context):
    #     return super().GetEntities(request, context)

    def resolve_entities(self, texts: List[str]):
        self.pipeline.process(texts)

    @staticmethod
    def __get_entities(text):
        nlp = spacy.load('en_core_web_sm')
        doc = nlp(text)

        response = EntityResponse()
        for ent in doc.ents:
            entity = NamedEntity()
            entity.label = ent.label_
            entity.text = ent.text
            response.entities.append(entity)

        return response
