import nltk

from core.async_data_pipeline import AsyncDataPipeline
from nltk.corpus import stopwords
from typing import List
from proto.nlp_pb2 import *

nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')
nltk.download('stopwords')

step = AsyncDataPipeline.step
step_per_item = AsyncDataPipeline.step_per_item


class AsyncNltkService:
    def __init__(self, entity_types):
        self.entity_resolution_pipeline = self.__entity_resolution_pipeline(entity_types)
        self.keywords_pipeline = self.__keywords_pipeline()

    def resolve_entities(self, texts: List[str]):
        self.entity_resolution_pipeline.process(texts)

    def find_keywords(self, texts):
        self.keywords_pipeline.process(texts)

    def __entity_resolution_pipeline(self, entity_types):
        self.entity_types = entity_types
        return AsyncDataPipeline(sinks=[
            step_per_item(nltk.sent_tokenize, sinks=[
                step(nltk.word_tokenize, sinks=[
                    step(nltk.pos_tag, sinks=[
                        step(nltk.ne_chunk, sinks=[
                            step(self.__parse_entities)
                        ])
                    ])
                ])
            ])
        ])

    def __keywords_pipeline(self):
        return AsyncDataPipeline(sinks=[
            step(self.__find_keywords)
        ])

    def __find_keywords(self, text):
        keywords = self.__parse_keywords(self.__parse_words(text))
        response = KeywordResponse()
        response.keywords.extend(keywords)
        print("=== keywords")
        print(response)

    def __parse_keywords(self, words):
        sw = stopwords.words("english")
        trans_table = str.maketrans("", "", "[]()-,|/\\\"?:;&%$#@!%^.")  # Remove these symbols from keywords
        keywords = [word.translate(trans_table) for word in words if word not in sw]
        return [k for k in keywords if k]

    def __parse_words(self, text):
        text = text.lower()
        text = text.replace('\n', '').replace('\r', '')
        return text.split(' ')

    def __parse_entities(self, chunks):
        response = EntityResponse()
        for entity_type in self.entity_types:
            leaves = self.__leaves(chunks, entity_type)
            for leaf in leaves:
                entity = NamedEntity()
                entity.label = entity_type
                entity.text = leaf[0][0]
                response.entities.append(entity)

        return response

    def __leaves(self, tree, label):
        return [t.leaves()
                for t in tree.subtrees(
                lambda s: s.label() == label)]
