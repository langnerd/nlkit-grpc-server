from typing import List

import nltk
from nltk.corpus import stopwords
from proto.nlp_pb2 import NamedEntity, EntityResponse
from proto.nlp_pb2_grpc import EntityServiceServicer, KeywordServiceServicer


class NltkService(EntityServiceServicer, KeywordServiceServicer):
    def __init__(self, entity_types: List[str]):
        self.entity_types = entity_types
        self.stopwords = stopwords.words('english')
        self.trans_table = str.maketrans("", "", "[]()-,|/\\\"?:;&%$#@!%^.")  # Remove these symbols from keywords

    def GetEntities(self, request, context):
        return self.get_entities(request.text)

    def GetKeywords(self, request, context):
        return self.get_keywords(request.text)

    def get_keywords(self, text):
        words = text.lower().replace('\n', '').replace('\r', '').split(' ')
        # We are removing all the stopwords and removing all other characters
        keywords = [word.translate(self.trans_table) for word in words if word not in self.stopwords]
        return sorted(list(filter(None, keywords)))

    def get_entities(self, text: str):
        tokens = nltk.word_tokenize(text)
        tags = nltk.pos_tag(tokens)
        chunks = nltk.ne_chunk(tags)

        response = EntityResponse()
        for entity_type in self.entity_types:
            leaves = self.__leaves(chunks, entity_type)
            for leaf in leaves:
                entity = NamedEntity()
                entity.label = entity_type
                entity.text = leaf[0][0]
                response.entities.append(entity)

        return response

    def __leaves(self, tree, label):
        return [t.leaves()
                for t in tree.subtrees(
                lambda s: s.label() == label)]