FROM python:3.7-slim

WORKDIR /app

# Install system libraries
RUN apt-get update && \
    apt-get install -y git && \
    apt-get install -y gcc

# Install project dependencies
COPY ./requirements.txt .

RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

# Download corpora
RUN python -m nltk.downloader stopwords punkt averaged_perceptron_tagger maxent_ne_chunker words
RUN python -m spacy download en_core_web_sm

# Don't use terminal buffering, print all to stdout / err right away
ENV PYTHONUNBUFFERED 1

#COPY .. .
