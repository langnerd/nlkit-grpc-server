#!/bin/bash
set -x

CWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Generate gRPC services
python \
  -m grpc_tools.protoc -I. \
  --python_out=. \
  --grpc_python_out=. \
  ./proto/*.proto

# Start the server
python "$CWD/server.py"

