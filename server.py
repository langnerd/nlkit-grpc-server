import time
import grpc
from concurrent import futures
from proto.nlp_pb2_grpc import *
from services.async_nltk_service import AsyncNltkService
from services.async_spacy_service import AsyncSpacyService
from services.nltk_service import NltkService


def serve(port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    nltk_service = NltkService(entity_types=[
            'DATE',
            'FACILITY',
            'GPE',  # Geo-political entity
            'LOCATION',
            'MONEY',
            'ORGANIZATION',
            'PERCENT',
            'PERSON',
            'TIME',
        ])

    add_EntityServiceServicer_to_server(
        nltk_service, server
    )

    add_KeywordServiceServicer_to_server(
        nltk_service, server
    )
    server.add_insecure_port('[::]:' + str(port))
    server.start()
    print("Listening on port {}..".format(port))
    try:
        while True:
            time.sleep(10000)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == "__main__":
    serve(6000)
