from services.async_nltk_service import AsyncNltkService
from services.async_spacy_service import AsyncSpacyService
from services.nltk_service import NltkService

text = """
    Babylon was a significant city in ancient Mesopotamia,
    in the fertile plain between the Tigris and Euphrates rivers.
    The city was built upon the Euphrates, and divided in equal parts along its left and right banks,
    with steep embankments to contain the river's seasonal floods.
    Hammurabi was the sixth Amorite king of Babylon
    from 1792 BC to 1750 BC middle chronology.
    He became the first king of the Babylonian Empire following the abdication of his father,
    Sin-Muballit, who had become very ill and died, extending Babylon's control over Mesopotamia
    by winning a series of wars against neighboring kingdoms.
    """

if __name__ == '__main__':
    # Commonly used entity types,
    # https://www.nltk.org/book/ch07.html
    nltk_service = AsyncNltkService(entity_types=[
        'DATE',
        'FACILITY',
        'GPE',  # Geo-political entity
        'LOCATION',
        'MONEY',
        'ORGANIZATION',
        'PERCENT',
        'PERSON',
        'TIME',
    ])
    # nltk_service.resolve_entities(texts)
    # nltk_service.find_keywords(texts)

    spacy_service = AsyncSpacyService()
    # spacy_service.resolve_entities(texts)

    service = NltkService(entity_types=[
        'DATE',
        'FACILITY',
        'GPE',  # Geo-political entity
        'LOCATION',
        'MONEY',
        'ORGANIZATION',
        'PERCENT',
        'PERSON',
        'TIME',
    ])

    entities = service.get_entities(text)
    print(entities)

    keywords = service.get_keywords(text)
    print(keywords)