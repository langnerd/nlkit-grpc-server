# NLKit gRPC Server
A gRPC Python server that integrates with various NLP libraries (nltk, spacy etc.) and maps the results into a unified low-level model
- includes NLP Python libraries that do all the text processing (nltk, spacy etc.)
- common definition file to define the data model
- NLP library specific services that implement the data model (nltk service, spacy service etc.)

## How to run
```
docker-compose up
```
