from asyncio import coroutine
from typing import List


class AsyncDataPipeline:

    def __init__(self, sinks):
        self.sinks = sinks

    def process(self, texts: List[str]):
        for text in texts:
            for sink in self.sinks:
                next(sink)
                sink.send(text)

    @staticmethod
    def next_step(data, sinks):
        for sink in sinks:
            next(sink)
            sink.send(data)

    @staticmethod
    @coroutine
    def step(f, sinks=None):
        if sinks is None:
            sinks = []
        while True:
            x = (yield)
            if x:
                y = f(x)
                AsyncDataPipeline.next_step(y, sinks)

    @staticmethod
    @coroutine
    def step_per_item(f, sinks=None):
        if sinks is None:
            sinks = []
        while True:
            x = (yield)
            if x:
                ys = f(x)
                for y in ys:
                    AsyncDataPipeline.next_step(y, sinks)

    @staticmethod
    @coroutine
    def printer():
        while True:
            line = (yield)
            print(line)
